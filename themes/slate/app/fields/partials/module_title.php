<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$module_title = new FieldsBuilder('module_title');

$module_title
		// Class
		->addText('module_title', [
				'label' => 'Module Title',
				'ui' => $config->ui
			])
    	->setInstructions('Add a unique module title to this instance');

return $module_title;