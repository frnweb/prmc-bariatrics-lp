<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$class = new FieldsBuilder('class');

$class
		// Class
		->addText('class', [
				'label' => 'Class',
				'ui' => $config->ui
			])
    	->setInstructions('Class field');

return $class;