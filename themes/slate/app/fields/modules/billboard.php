<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],

];

$billboard = new FieldsBuilder('billboard');

$billboard
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));


$billboard
	
	->addTab('content', ['placement' => 'left'])
		->addGroup('billboard')

			//Content
			->addGroup('billboard_content', ['wrapper' => ['width' => 50]])

				// Header
				->addText('header', [
						'label' => 'Header',
					])

				// WYSIWYG
	            ->addWysiwyg('paragraph', [
	                'label' => 'Paragraph',
	            ])

            ->endGroup()
                

			//Image 
			->addGroup('billboard_image', ['wrapper' => ['width' => 50]])

				//Large Image 
				->addImage('large')
					->setInstructions('Image background for billboard medium breakpoint and up')

				//Small Image 
				->addImage('small')
					->setInstructions('Image background for billboard small breakpoint')
				
			->endGroup()
			
			//Button
			->addFields(get_field_partial('modules.button'))
			
	   ->endGroup();

return $billboard;