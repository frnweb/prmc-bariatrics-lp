<?php
// CALLOUT CARD
	function sl_callout_card ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> '',
			'img'		=> ''
			), $atts );
		$content = wpautop(trim($content));
		return '<div class="sl_callout sl_callout--card ' . esc_attr($specs['class'] ) . '"><div class="sl_callout--card__media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="sl_callout--card__content">' . do_shortcode ( $content ) . '</div></div>';
	}
	add_shortcode ('callout-card', 'sl_callout_card' );
///CALLOUT CARD
?>