<?php
	// CALLOUT LINK
		function sl_callout_link ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'target'	=> '_self',
				'modal'		=> ''
				), $atts );

				if(esc_attr($specs['modal'] ) != '') {
					$linkTarget = 'data-open="' . esc_attr($specs['modal'] ) . '"';
				} else {
					$linkTarget = 'href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'"';
				};
				$content = wpautop(trim($content));
				$calloutLink = '<a class="sl_callout-link" ' . $linkTarget . '>' . do_shortcode( $content ) . '</a>';

				return '[shortcode_unautop]' . $calloutLink .'[/shortcode_unautop]';
		}

		add_shortcode ('callout-link', 'sl_callout_link' );
	///CALLOUT LINK

?>